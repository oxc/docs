Title: Documentation
CSS: /css/main.css

#{include head}

# Documentation

## Contributor documentation
* EAPI
    * [Exheres for smarties](eapi/exheres-for-smarties.html)
    * [Alternatives](eapi/alternatives.html)
    * [Providers and Virtuals](eapi/providers-and-virtuals.html)
* Exlibs
    * [flag-o-matic.exlib](exlibs/flag-o-matic.html)
    * [scm.exlib](exlibs/scm.html)
    * [toolchain-funcs.exlib](exlibs/toolchain-funcs.html)
* [Bootstrapping](bootstrapping.html)
* [Contributing](contributing.html)
* [Cross-compiling support in exhereses](cross.html)
* [Emacs](emacs.html)
* [Gitlab](gitlab.html)
* [Graveyard](graveyard.html)
* [Mirrors](mirrors.html)
* [Multiarch for developers](multiarch.txt)
* [Multiarch TODO list](multiarch-TODO.html)
* [Multibuild for developers](multibuild.html)
* [Project ideas](project-ideas.html)
* [Using the patchbot](patchbot.html)
* [Workflow](workflow.html)

## User documentation
* [cave](http://paludis.exherbo.org/clients/cave.html)
* [Configuration](http://paludis.exherbo.org/configuration/index.html)
* [Expectations](expectations.html)
* [Features](features.html)
* [FAQ](faq.html)
* [Install Guide](install-guide.html)
* [KDE on Exherbo](kde.html)
* [Multiarch Migration Guide](multiarch/multiarch.html)
* [Multiarch: Adding another target architecture](multiarch/multiarch-new-target.html)
* [Paludis-FAQ](http://paludis.exherbo.org/faq/index.html)
* [Systemd HowTo](systemd.html)
* [Unavailable repository](//ciaranm.wordpress.com/2008/06/12/dealing-with-lots-of-repositories/)
* [Unwritten repository](//ciaranm.wordpress.com/2008/10/06/dealing-with-unwritten-packages/)
* [Without systemd](without-systemd.html)

#{include foot}

<!-- vim: set tw=100 ft=mkd spell spelllang=en sw=4 sts=4 et : -->
